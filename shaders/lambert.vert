attribute vec3 aVertexPosition;
attribute vec4 aVertexColor;
attribute vec3 aVertexNormal;

uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;
uniform mat3 uNormalMatrix;

uniform vec3 uAmbientLightColor;
uniform vec3 uPointLightPosition;
uniform vec3 uPointLightDiffuseColor;
uniform vec3 uPointLightSpecularColor;

uniform vec3 uAttenuationCoefficients;

varying lowp vec4 vColor;
varying vec3 vLightWeighting;

const float shininess = 16.0;
const float power = 30.;


void main(void) {

    float distance    = length(uPointLightPosition - aVertexPosition);
    float attenuation = power / (uAttenuationCoefficients[0] + distance * uAttenuationCoefficients[1] + distance * distance * uAttenuationCoefficients[2]);
    vec4 vertexPositionEye4 = uModelViewMatrix * vec4(aVertexPosition, 1.0);
    vec3 vertexPositionEye3 = vertexPositionEye4.xyz / vertexPositionEye4.w;

    vec3 lightDirection = normalize(uPointLightPosition - vertexPositionEye3);

    vec3 normal = normalize(uNormalMatrix * aVertexNormal);

    float diffuseLightDot = max(dot(normal, lightDirection) *0.5, 0.0);

    // получаем вектор отраженного луча и нормализуем его
    vec3 reflectionVector = normalize(reflect(-lightDirection, normal));

    // установка вектора камеры
    vec3 viewVectorEye = -normalize(vertexPositionEye3);
    float specularLightDot = max(dot(reflectionVector, viewVectorEye), 0.0);
    float specularLightParam = pow(specularLightDot, shininess);

    // А это для Ламберта
    vLightWeighting = uPointLightDiffuseColor * diffuseLightDot;
    // Finally transform the geometry
    vLightWeighting = vLightWeighting * attenuation;
    gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(aVertexPosition, 1.0);
    vColor = aVertexColor;
}
