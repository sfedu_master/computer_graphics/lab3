precision mediump float;

varying vec3 vLightWeighting;

varying lowp vec4 vColor;
void main(void) {
    gl_FragColor = vec4(vLightWeighting * vColor.rgb, vColor.a);

}
