import cubeFragmentShader from './shaders/cube.frag';
import cubeVertexShader from './shaders/lambert.vert';
import {clearScreen, createProgramInfo,degrees_to_radians} from "./service";
import {draw_scene} from "./scene";

export const shaders = {
    vertex:cubeVertexShader,
    fragment:cubeFragmentShader
}
let programInfo = null
//GLOBALS
let gl = null
let scene_rotation = 0
let ensemble_rotation = 0
let cube_rotation = 0


function onKeyPress(event) {
    const { key } = event
    if (key === "ArrowRight") {
        scene_rotation = (scene_rotation + 1)%360
    } else if (key === "ArrowLeft") {
        ensemble_rotation = (ensemble_rotation + 1)%360
    }
    else if (key === "ArrowUp")
    {
        cube_rotation = (cube_rotation + 1)%360
    }

}

document.addEventListener('keydown', onKeyPress)

function render()
{

    clearScreen(gl)
    //console.log(scene_rotation)

    //Setup the information of the scene.
    //Rotations of figures.
    programInfo.rotations.scene_rotation = degrees_to_radians(scene_rotation)
    programInfo.rotations.ensemble_rotation = degrees_to_radians(ensemble_rotation)
    programInfo.rotations.cube_rotation = degrees_to_radians(cube_rotation)


    //Perspective Matrix Creation
    draw_scene(gl,programInfo)

    requestAnimationFrame(render)
}

function main()
{
    //Initializing context
    const canvas = document.getElementById('canvas')
    gl = canvas.getContext('webgl')
    if (gl === null) {
        alert("Unable to initialize WebGL. Your browser or machine may not support it.");
        return;
    }
    programInfo = createProgramInfo(gl)
    requestAnimationFrame(render)
}

main()