import {drawCube} from "./cube";

function initializeMatrices(gl,programInfo)
{
    // Projection matrix creation
    const fieldOfView = 45 * Math.PI / 180;   // in radians
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();
    mat4.perspective(projectionMatrix,
        fieldOfView,
        aspect,
        zNear,
        zFar)

    //Model View Matrix creation
    const modelViewMatrix = mat4.create();
    mat4.translate(modelViewMatrix,modelViewMatrix,[0,0,-5])
    mat4.rotate(modelViewMatrix,  // destination matrix
        modelViewMatrix,  // matrix to rotate
        programInfo.rotations.scene_rotation,   // amount to rotate in radians
        [0, 1, 0]);

    //Normal Matrix
    const normalMatrix = mat3.create()
    mat3.identity(normalMatrix)
    mat3.normalFromMat4(normalMatrix,modelViewMatrix);

    programInfo.modelViewMatrix = modelViewMatrix
    programInfo.projectionMatrix = projectionMatrix
    programInfo.normalMatrix = normalMatrix
    //console.log(normalMatrix)

}

function initializeLights(gl,programInfo)
{
    gl.uniform3fv(programInfo.uniformLocations.ambientLightColor, [
        programInfo.ambientLight.r,
        programInfo.ambientLight.g,
        programInfo.ambientLight.b,]);
    gl.uniform3fv(programInfo.uniformLocations.pointLightPosition, [
        programInfo.pointLight.position.x,
        programInfo.pointLight.position.y,
        programInfo.pointLight.position.z]);
    gl.uniform3fv(programInfo.uniformLocations.pointLightDiffuseColor, [
        programInfo.pointLight.diffuse_colour.r,
        programInfo.pointLight.diffuse_colour.g,
        programInfo.pointLight.diffuse_colour.b]);
    gl.uniform3fv(programInfo.uniformLocations.pointLightSpecularColor, [
        programInfo.pointLight.specular_colour.r,
        programInfo.pointLight.specular_colour.g,
        programInfo.pointLight.specular_colour.b]);
    gl.uniform3fv(programInfo.uniformLocations. attenuationCoefficients,[
        programInfo.attenuationCoefficients.constant,
        programInfo.attenuationCoefficients.linear,
        programInfo.attenuationCoefficients.quadratic
    ])


}

export function draw_scene(gl,programInfo)
{

    initializeMatrices(gl,programInfo)
    initializeLights(gl,programInfo)
    //Rotate whole scene
    mat4.rotate(programInfo.modelViewMatrix,  // destination matrix
        programInfo.modelViewMatrix,  // matrix to rotate
        programInfo.rotations.scene_rotation,   // amount to rotate in radians
        [0, 1, 0]);

    // Move to pedestal
    mat4.translate(programInfo.modelViewMatrix,programInfo.modelViewMatrix,[0,0,-30])
    mat4.rotate(programInfo.modelViewMatrix,  // destination matrix
        programInfo.modelViewMatrix,  // matrix to rotate
        programInfo.rotations.ensemble_rotation,   // amount to rotate in radians
        [0, 1, 0]);

    // Move to small cubes.
    mat4.translate(programInfo.modelViewMatrix,programInfo.modelViewMatrix,[3,0,0])
    mat4.rotate(programInfo.modelViewMatrix,  // destination matrix
        programInfo.modelViewMatrix,  // matrix to rotate
        programInfo.rotations.cube_rotation,   // amount to rotate in radians
        [0, 1, 0]);
    mat3.normalFromMat4(programInfo.normalMatrix,programInfo.modelViewMatrix);
    drawCube(gl,programInfo,[0.8,0.5,0.2])
    mat4.rotate(programInfo.modelViewMatrix,  // destination matrix
        programInfo.modelViewMatrix,  // matrix to rotate
        -programInfo.rotations.cube_rotation,   // amount to rotate in radians
        [0, 1, 0]);
    mat4.translate(programInfo.modelViewMatrix,programInfo.modelViewMatrix,[-2,0.5,0])
    mat4.scale(programInfo.modelViewMatrix,programInfo.modelViewMatrix,[1,1.5,1])
    mat4.rotate(programInfo.modelViewMatrix,  // destination matrix
        programInfo.modelViewMatrix,  // matrix to rotate
        programInfo.rotations.cube_rotation,   // amount to rotate in radians
        [0, 1, 0]);
    mat3.normalFromMat4(programInfo.normalMatrix,programInfo.modelViewMatrix);
    drawCube(gl,programInfo,[0.76,0.79,0.71])
    mat4.rotate(programInfo.modelViewMatrix,  // destination matrix
        programInfo.modelViewMatrix,  // matrix to rotate
        -programInfo.rotations.cube_rotation,   // amount to rotate in radians
        [0, 1, 0]);
    mat4.scale(programInfo.modelViewMatrix,programInfo.modelViewMatrix,[1,1.5,1])
    mat4.translate(programInfo.modelViewMatrix,programInfo.modelViewMatrix,[-2,0.330,0])
    mat4.rotate(programInfo.modelViewMatrix,  // destination matrix
        programInfo.modelViewMatrix,  // matrix to rotate
        programInfo.rotations.cube_rotation,   // amount to rotate in radians
        [0, 1, 0]);
    mat3.normalFromMat4(programInfo.normalMatrix,programInfo.modelViewMatrix);
    drawCube(gl,programInfo,[1,0.84,0])

}