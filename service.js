import {shaders} from "./index";

export function degrees_to_radians(degrees)
{
    var pi = Math.PI;
    return degrees * (pi/180);
}

function loadShader(gl, type, source) {
    const shader = gl.createShader(type);
    // Send the source to the shader object
    gl.shaderSource(shader, source);
    // Compile the shader program
    gl.compileShader(shader);
    // See if it compiled successfully

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        console.error('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }

    return shader;
}

function initShaderProgram(gl) {
    const vertexShader = loadShader(gl,gl.VERTEX_SHADER, shaders.vertex)
    const fragmentShader = loadShader(gl,gl.FRAGMENT_SHADER,shaders.fragment)

    const shaderProgram = gl.createProgram()
    gl.attachShader(shaderProgram, vertexShader)
    gl.attachShader(shaderProgram, fragmentShader)
    gl.linkProgram(shaderProgram)

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
        return null;
    }

    return shaderProgram;
}

export function createProgramInfo(gl)
{
    let shaderProgram = initShaderProgram(gl)
    let programInfo = {
        program: shaderProgram,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(shaderProgram,'aVertexPosition'),
            vertexColor : gl.getAttribLocation(shaderProgram, 'aVertexColor'),
            vertexNormal: gl.getAttribLocation(shaderProgram,'aVertexNormal'),

        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
            normalMatrix: gl.getUniformLocation(shaderProgram,'uNormalMatrix'),
            ambientLightColor: gl.getUniformLocation(shaderProgram, 'uAmbientLightColor'),
            pointLightPosition: gl.getUniformLocation(shaderProgram, 'uPointLightPosition'),
            pointLightDiffuseColor: gl.getUniformLocation(shaderProgram,'uPointLightDiffuseColor'),
            pointLightSpecularColor: gl.getUniformLocation(shaderProgram,'uPointLightSpecularColor'),
            attenuationCoefficients: gl.getUniformLocation(shaderProgram,'uAttenuationCoefficients')
        },
        rotations:
            {
                scene_rotation: 0.0,
                ensemble_rotation: 0.0,
                cube_rotation: 0.0
            },
        pointLight:{
            position: {
                x:0,
                y:0,
                z:-25
            },
            diffuse_colour: {
                r:0.7,
                g:0.7,
                b:1
            },
            specular_colour:{
                r:0.3,
                g:0.3,
                b:0.3
            }

        },
        ambientLight:{
            r: 0.3,
            g: 0.3,
            b: 0.3,
        },
        attenuationCoefficients:
            {
                constant: 0.,
                linear: 1,
                quadratic: 0.
            }
    }
    return programInfo
}

export function clearScreen(gl) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    gl.clearDepth(1.0);                 // Clear everything
    gl.enable(gl.DEPTH_TEST);           // Enable depth testing
    gl.depthFunc(gl.LEQUAL);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
}

